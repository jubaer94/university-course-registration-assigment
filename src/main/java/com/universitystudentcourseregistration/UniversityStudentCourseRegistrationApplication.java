package com.universitystudentcourseregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class UniversityStudentCourseRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityStudentCourseRegistrationApplication.class, args);
	}

}
