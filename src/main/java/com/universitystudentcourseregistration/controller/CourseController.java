package com.universitystudentcourseregistration.controller;

import com.universitystudentcourseregistration.dto.CourseDto;
import com.universitystudentcourseregistration.dto.DepartmentDto;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.service.CourseService;
import com.universitystudentcourseregistration.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/course")
public class CourseController {

    private final CourseService courseService;
    private final DepartmentService departmentService;
    @Autowired
    public CourseController(CourseService courseService, DepartmentService departmentService) {
        this.courseService = courseService;
        this.departmentService = departmentService;
    }
    @GetMapping("/list")
    public String list(Model model)
    {
        List<CourseDto> courseDtoList = courseService.findAll();
        model.addAttribute("courseDtoList" , courseDtoList);

        return "course/course-list";

    }
    @GetMapping("/add")
    public String addForm(Model model)
    {
        CourseDto courseDto = new CourseDto();
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("courseDto" ,courseDto);
        model.addAttribute("departmentDtoList" , departmentDtoList);
        return "course/course-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute CourseDto courseDto, Model model)
    {

        courseService.save(courseDto);
        return "redirect:/course/list";

    }
    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id") long id, Model model)
    {
        CourseDto courseDto = courseService.findById(id);
        model.addAttribute("courseDto" , courseDto);
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("departmentDtoList" , departmentDtoList);

        return "course/course-form";
    }
    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") long id, Model model)
    {
        CourseDto courseDto = courseService.findById(id);
        model.addAttribute("courseDto" , courseDto);
        return "course/view-course";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id)
    {
        courseService.deleteById(id);
        return "redirect:/course/list";
    }
}
