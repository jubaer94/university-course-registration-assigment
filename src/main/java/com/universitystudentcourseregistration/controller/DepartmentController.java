package com.universitystudentcourseregistration.controller;

import com.universitystudentcourseregistration.dto.DepartmentDto;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.service.DepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/list")
    public String list(Model model)
    {
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("departmentDtoList" , departmentDtoList);

        return "department/department-list";

    }
    @GetMapping("/add")
    public String addForm(Model model)
    {
        DepartmentDto departmentDto = new DepartmentDto();
        model.addAttribute("departmentDto" ,departmentDto);
        return "department/department-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute DepartmentDto departmentDto, Model model)
    {

        departmentService.save(departmentDto);
        return "redirect:/department/list";

    }
    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id") long id, Model model)
    {
        DepartmentDto departmentDto = departmentService.findById(id);
        model.addAttribute("departmentDto" , departmentDto);
        return "department/department-form";
    }
    @GetMapping("/view/{id}")
    public String viewDepartment(@PathVariable("id") long id, Model model)
    {
        DepartmentDto departmentDto = departmentService.findById(id);
        model.addAttribute("departmentDto" , departmentDto);
        return "department/view-department";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id)
    {
       departmentService.deleteById(id);
       return "redirect:/department/list";
    }
    @GetMapping(value = "/getAll", consumes = "application/json", produces = "application/json")
    public @ResponseBody List<DepartmentDto> getAllDepartment_onLoad() {
        return departmentService.findAll();
    }

    @PostMapping(value = "/ajax/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody DepartmentDto saveDepartment(@RequestBody DepartmentDto departmentDto) {
//        Department department = new Department();
//        BeanUtils.copyProperties(departmentDto, department);
//        departmentService.saveDepartment(department);
//        department = departmentService.getDepartmentById(department.getDepartmentId());
//        BeanUtils.copyProperties(department, departmentDto);
        DepartmentDto departmentDto1 = departmentService.save(departmentDto);
        return departmentDto1;
    }

}
