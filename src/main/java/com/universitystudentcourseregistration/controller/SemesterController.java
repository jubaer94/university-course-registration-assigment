package com.universitystudentcourseregistration.controller;

import com.universitystudentcourseregistration.dto.CourseDto;
import com.universitystudentcourseregistration.dto.DepartmentDto;
import com.universitystudentcourseregistration.dto.SemesterDto;
import com.universitystudentcourseregistration.dto.StudentDto;
import com.universitystudentcourseregistration.service.CourseService;
import com.universitystudentcourseregistration.service.DepartmentService;
import com.universitystudentcourseregistration.service.SemesterService;
import com.universitystudentcourseregistration.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/semester")
public class SemesterController {
    private final DepartmentService departmentService;
    private  final CourseService courseService ;
    private final SemesterService semesterService;
    private  final StudentService studentService;

    @Autowired
    public SemesterController(DepartmentService departmentService, CourseService courseService, SemesterService semesterService, StudentService studentService) {
        this.departmentService = departmentService;
        this.courseService = courseService;
        this.semesterService = semesterService;
        this.studentService = studentService;
    }

    @GetMapping("/list")
    public String list(Model model)
    {
        List<SemesterDto> semesterDtoList = semesterService.findAll();
        model.addAttribute("semesterDtoList" , semesterDtoList);

        return "semester/semester-list";

    }
    @GetMapping("/add")
    public String addForm(Model model)
    {
        SemesterDto semesterDto = new SemesterDto();
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("semesterDto" ,semesterDto);
        model.addAttribute("departmentDtoList" , departmentDtoList);
        return "semester/semester-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute SemesterDto semesterDto, Model model)
    {

        semesterService.save(semesterDto);
        return "redirect:/semester/list";

    }
    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id") long id, Model model)
    {
        SemesterDto semesterDto = semesterService.findById(id);
        model.addAttribute("semesterDto" , semesterDto);
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("departmentDtoList" , departmentDtoList);

        return "semester/semester-form";
    }
    @GetMapping("/view/{id}")
    public String viewSemester(@PathVariable("id") long id, Model model)
    {
        SemesterDto semesterDto = semesterService.findById(id);
        model.addAttribute("semesterDto" , semesterDto);

        List<CourseDto> courseDtoList = courseService.findAll();
        model.addAttribute("courseDtoList" , courseDtoList);

        List<Long> listOfCourseId=new ArrayList<Long>();

        semesterDto.getCourseList().forEach(o->{
            listOfCourseId.add(o.getId());
        });
        model.addAttribute("listOfCourseId" , listOfCourseId);
        List<StudentDto> studentDtoList = studentService.findAll();
        model.addAttribute("studentDtoList" , studentDtoList);

        return "semester/view-semester";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id)
    {
        semesterService.deleteById(id);
        return "redirect:/semester/list";
    }
    @GetMapping("/add-course/{id}")
    public String showAddCourseForm(@PathVariable("id") long id, Model model)
    {
        SemesterDto semesterDto = semesterService.findById(id);
        model.addAttribute("semesterDto" , semesterDto);
        List<CourseDto> courseDtoList = courseService.findAll();
        model.addAttribute("courseDtoList" , courseDtoList);

        return "semester/add-course";
    }
    @PostMapping("/add-course")
    public String saveTheCourseToSemester(@ModelAttribute SemesterDto semesterDto)
    {
            SemesterDto semesterDto1= semesterService.findById(semesterDto.getId());
            semesterDto1.setCourseList(semesterDto.getCourseList());
            semesterService.saveCoursesToSemester(semesterDto1);
        return "redirect:/semester/list";
    }

}
