package com.universitystudentcourseregistration.controller;

import com.universitystudentcourseregistration.dto.CourseDto;
import com.universitystudentcourseregistration.dto.DepartmentDto;
import com.universitystudentcourseregistration.dto.SemesterDto;
import com.universitystudentcourseregistration.dto.StudentDto;
import com.universitystudentcourseregistration.service.CourseService;
import com.universitystudentcourseregistration.service.DepartmentService;
import com.universitystudentcourseregistration.service.SemesterService;
import com.universitystudentcourseregistration.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    private final DepartmentService departmentService;
    private  final CourseService courseService ;
    private final SemesterService semesterService;
    private final StudentService studentService ;

    public StudentController(DepartmentService departmentService, CourseService courseService, SemesterService semesterService, StudentService studentService) {
        this.departmentService = departmentService;
        this.courseService = courseService;
        this.semesterService = semesterService;
        this.studentService = studentService;
    }
    @GetMapping("/list")
    public String list(Model model)
    {
        List<StudentDto> studentDtoList = studentService.findAll();
        model.addAttribute("studentDtoList" , studentDtoList);

        return "student/student-list";

    }
    @GetMapping("/add")
    public String addForm(Model model)
    {
        StudentDto studentDto = new StudentDto();
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("studentDto" ,studentDto);
        model.addAttribute("departmentDtoList" , departmentDtoList);
        return "student/student-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute StudentDto studentDto, Model model)
    {

        studentService.save(studentDto);
        return "redirect:/student/list";

    }
    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id") long id, Model model)
    {
        StudentDto studentDto = studentService.findById(id);
        model.addAttribute("studentDto" , studentDto);
        List<DepartmentDto> departmentDtoList = departmentService.findAll();
        model.addAttribute("departmentDtoList" , departmentDtoList);

        return "student/student-form";
    }
    @GetMapping("/view/{id}")
    public String viewStudent(@PathVariable("id") long id, Model model)
    {
        StudentDto studentDto = studentService.findById(id);
        model.addAttribute("studentDto" , studentDto);


        return "student/view-student";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id)
    {
        studentService.deleteById(id);
        return "redirect:/student/list";
    }
    @GetMapping("/add-semester/{id}")
    public String showAddSemesterForm(@PathVariable("id") long id, Model model)
    {
        StudentDto studentDto = studentService.findById(id);
        System.out.println(studentDto.getDepartment().getId());
        model.addAttribute("studentDto" , studentDto);
        List<SemesterDto> semesterDtoList = semesterService.findAll();
        model.addAttribute("semesterDtoList" , semesterDtoList);

        return "student/add-semester";
    }
    @PostMapping("/add-semester")
    public String saveTheCourseToStudent(@ModelAttribute StudentDto studentDto)
    {
        StudentDto studentDto1= studentService.findById(studentDto.getId());
        studentDto1.setSemester(studentDto.getSemester());
        studentService.saveSemesterToStudent(studentDto1);
        return "redirect:/student/list";
    }
    @GetMapping("/add-extra-course/{id}")
    public String showAddExtraCourseForm(@PathVariable("id") long id, Model model)
    {
        StudentDto studentDto = studentService.findById(id);
        System.out.println(studentDto.getDepartment().getId());
        model.addAttribute("studentDto" , studentDto);
        List<SemesterDto> semesterDtoList = semesterService.findAll();
        List<CourseDto> courseDtoList = courseService.findAll();
        model.addAttribute("semesterDtoList" , semesterDtoList);
        model.addAttribute("courseDtoList" , courseDtoList);

        return "student/add-extra-courses";
    }

    @PostMapping("/add-extra-course")
    public String saveTheExtraCoursesToStudent(@ModelAttribute StudentDto studentDto)
    {
        StudentDto studentDto1= studentService.findById(studentDto.getId());
        studentDto1.setExtraCourseList(studentDto.getExtraCourseList());
        studentService.saveExtraCoursesToStudent(studentDto1);
        return "redirect:/student/list";
    }

}
