package com.universitystudentcourseregistration.converter;

import com.universitystudentcourseregistration.dto.CourseDto;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.repository.CourseRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CourseDtoConverter implements Converter<String , CourseDto> {



    public CourseDtoConverter() {
    }

    @Override
    public CourseDto convert(String id) {


        CourseDto courseDto = new CourseDto();
        Long id1 =Long.parseLong(id);
        courseDto.setId(id1);
        return courseDto;
    }
}
