package com.universitystudentcourseregistration.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.universitystudentcourseregistration.entity.Semester;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class CourseDto implements Serializable {


    private long id;

    private String name;

    private boolean is_deleted=false;

    private SemesterDto semester;

    List<StudentDto> studentList;

    private DepartmentDto department;

    @Override
    public String toString() {
        return "CourseDto{" +
                "name='" + name + '\'' +
                '}';
    }



}
