package com.universitystudentcourseregistration.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.universitystudentcourseregistration.entity.Semester;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class DepartmentDto implements Serializable {

    private long id;

    private String name;

    private boolean is_deleted=false;

    private List<SemesterDto> semesters;

    private List<StudentDto> studentList;
}
