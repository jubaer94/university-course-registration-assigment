package com.universitystudentcourseregistration.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.entity.Student;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class SemesterDto implements Serializable {

    private long id;

    private String name;

    private boolean is_deleted=false;

    private List<Student> studentList;

    private DepartmentDto department;

    private List<CourseDto> courseList;

    private List<StudentDto> previousEnrolledStudents;

   // private List<CourseDto> courseDtoIdList;

//    public List<CourseDto> getCourseDtoIdList() {
//        return courseDtoIdList;
//    }
//
//    public void setCourseDtoIdList(List<CourseDto> courseDtoIdList) {
//        List<Long> idList=new ArrayList<>();
//
//        this.courseDtoIdList = courseDtoIdList;
//    }


    @Override
    public String toString() {
        return "SemesterDto{" +
                "name='" + name + '\'' +
                '}';
    }

}
