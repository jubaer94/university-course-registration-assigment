package com.universitystudentcourseregistration.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Semester;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class StudentDto implements Serializable {

    private long id;

    private String name;

    private boolean is_deleted=false;

    private SemesterDto semester;

    private  DepartmentDto department;

   private List<CourseDto> courseList;
    private List<SemesterDto> completedSemesters;
    private List<CourseDto> extraCourseList;

    List<Long> idList = new ArrayList<Long>();
    List<Long> extraCourseIdList = new ArrayList<Long>();


    public List<Long> get_completedSemesters_idList()
    {

        completedSemesters.forEach(o->{
            idList.add(o.getId());
        });
        return idList;
    }
    public List<Long> get_extraCourseIdList()
    {

        extraCourseList.forEach(o->{
            extraCourseIdList.add(o.getId());
        });
        return extraCourseIdList;
    }

}
