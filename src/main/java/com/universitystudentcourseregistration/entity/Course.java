package com.universitystudentcourseregistration.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "course")

public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    boolean active = true;
    @ManyToOne
    @JoinTable(name = "semester_course",
            joinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "semester_id", referencedColumnName = "id")}
    )
    private Semester semester;
    @ManyToMany(mappedBy = "extraCourseList")
    List<Student> studentListOfOutOfSemester;
    @ManyToOne
    @JoinTable(name = "department_course",
            joinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "department_id", referencedColumnName = "id")}
    )
    private Department department;
}
