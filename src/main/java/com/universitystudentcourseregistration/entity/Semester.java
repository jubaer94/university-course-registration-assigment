package com.universitystudentcourseregistration.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "semester")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Semester {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    boolean active = true;
    @OneToMany
    @JoinTable(name = "student_semester",
            joinColumns = {@JoinColumn(name = "semester_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id", referencedColumnName = "id")}
    )
    List<Student> studentList;
    @ManyToOne
    @JoinTable(
            name = "department_semester",
            joinColumns = {@JoinColumn(name = "semester_id")},
            inverseJoinColumns = {@JoinColumn(name ="department_id")}
    )
    private  Department department;
    @OneToMany
    @JoinTable(name = "semester_course",
            joinColumns = {@JoinColumn(name = "semester_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")}
    )
    private List<Course> courseList;

    @ManyToMany(mappedBy = "extraCourseList")
    List<Student> previousEnrolledStudents;


}
