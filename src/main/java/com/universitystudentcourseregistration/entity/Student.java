package com.universitystudentcourseregistration.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "student")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    boolean active = true;
    @ManyToOne
    @JoinTable(name = "student_semester",
            joinColumns = {@JoinColumn(name = "student_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "semester_id", referencedColumnName = "id")}
    )
    private Semester semester;

    @ManyToOne
    @JoinTable(name = "department_student",
            joinColumns = {@JoinColumn(name = "student_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "department_id", referencedColumnName = "id")}
    )

    private Department department;

    @ManyToMany
    @JoinTable(
            name = "student_extra_course",
            joinColumns = @JoinColumn(name = "student_id" , referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "course_id" ,referencedColumnName = "id"))
    private List<Course> extraCourseList;
    @ManyToMany
    @JoinTable(
            name = "student_completed_semester",
            joinColumns = @JoinColumn(name = "student_id" ,referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "semester_id" ,referencedColumnName = "id"))
    private List<Semester> completedSemesters;

}
