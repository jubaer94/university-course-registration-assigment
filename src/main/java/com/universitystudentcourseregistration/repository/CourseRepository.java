package com.universitystudentcourseregistration.repository;

import com.universitystudentcourseregistration.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course , Long> {

    List<Course> findByActiveTrue();
}
