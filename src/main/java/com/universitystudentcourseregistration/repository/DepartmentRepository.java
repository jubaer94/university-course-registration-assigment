package com.universitystudentcourseregistration.repository;

import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department , Long> {

    List<Department> findByActiveTrue();
}
