package com.universitystudentcourseregistration.repository;

import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Semester;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SemesterRepository extends JpaRepository<Semester , Long> {

    List<Semester> findByActiveTrue();
}
