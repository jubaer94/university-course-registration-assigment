package com.universitystudentcourseregistration.repository;

import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student ,Long> {

    List<Student> findByActiveTrue();
}
