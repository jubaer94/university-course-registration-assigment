package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.CourseDto;

import java.util.List;

public interface CourseService {

    public List<CourseDto> findAll();

    public CourseDto findById(long theId);

    public void save(CourseDto courseDto);

    public void deleteById(long theId);
}
