package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.CourseDto;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.repository.CourseRepository;
import com.universitystudentcourseregistration.repository.DepartmentRepository;
import com.universitystudentcourseregistration.repository.SemesterRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;
    private SemesterRepository semesterRepository;
    private DepartmentRepository departmentRepository;
    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, SemesterRepository semesterRepository, DepartmentRepository departmentRepository) {
        this.courseRepository = courseRepository;
        this.semesterRepository = semesterRepository;
        this.departmentRepository = departmentRepository;
    }

    ModelMapper modelMapper = new ModelMapper();
    @Override
    public List<CourseDto> findAll() {
        List<Course> courseList = courseRepository.findByActiveTrue();
        List<CourseDto> courseDtoList = new ArrayList<>();

        for (Course course :courseList)
        {
            CourseDto courseDto = modelMapper.map(course , CourseDto.class);
            courseDtoList.add(courseDto);

        }
        return courseDtoList;

    }

    @Override
    public CourseDto findById(long theId) {

        Optional<Course> result = courseRepository.findById(theId);
        if (result.isPresent())
        {
            Course course = result.get();
            CourseDto courseDto = modelMapper.map(course , CourseDto.class);
            return courseDto ;
        }
        else {
            throw new RuntimeException(" course not found with id = " +theId);
        }


    }

    @Override
    public void save(CourseDto courseDto) {
//        Course course = modelMapper.map(courseDto , Course.class);

        Course course;
        course = modelMapper.map(courseDto, Course.class);
        courseRepository.save(course);
//        if (courseDto.getId()>0)
//       {
////           course = courseRepository.getOne(courseDto.getId());
////           Department department = departmentRepository.getOne(courseDto.getDepartment().getId());
////           course.setDepartment(department);
////           course.setName(courseDto.getName());
////           courseRepository.save(course);
//           course = modelMapper.map(courseDto, Course.class);
//           courseRepository.save(course);
//
//       }
//       else if (courseDto.getId()<=0){
//            course = modelMapper.map(courseDto, Course.class);
//            courseRepository.save(course);
//        }



    }

    @Override
    public void deleteById(long theId) {
        Optional<Course> result = courseRepository.findById(theId);
        if (result.isPresent())
        {
            Course course = result.get();
            course.setActive(false);
            course.setSemester(null);
            course.setDepartment(null);
//            course.setStudentList(null);
            courseRepository.save(course);
        }
        else {
            throw  new RuntimeException("delete not possible employee not found by the id = " +theId);
        }


    }
}
