package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.DepartmentDto;

import java.util.List;

public interface DepartmentService {
    public List<DepartmentDto> findAll();

    public DepartmentDto findById(long theId);

    public DepartmentDto save(DepartmentDto departmentDto);

    public void deleteById(long theId);
}
