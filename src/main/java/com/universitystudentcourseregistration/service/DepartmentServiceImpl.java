package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.DepartmentDto;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.repository.DepartmentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private DepartmentRepository departmentRepository;
    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<DepartmentDto> findAll() {
        List<Department> departments = departmentRepository.findByActiveTrue();
        List<DepartmentDto> departmentDtoList = new ArrayList<>();
        for (Department department : departments)
        {
            DepartmentDto departmentDto = modelMapper.map(department , DepartmentDto.class);
//
//            DepartmentDto departmentDto = new DepartmentDto();
//            BeanUtils.copyProperties(department , departmentDto);


            departmentDtoList.add(departmentDto);
        }
        return departmentDtoList;
    }

    @Override
    public DepartmentDto findById(long theId) {
        Optional<Department> result = departmentRepository.findById(theId);
        if (result.isPresent())
        {
            Department department = result.get();
            DepartmentDto departmentDto = modelMapper.map(department , DepartmentDto.class);
            return departmentDto;
        }
        else {
            throw new RuntimeException("the department not found by the id " + theId);
        }

    }

    @Override
    public DepartmentDto save(DepartmentDto departmentDto) {

        DepartmentDto departmentDto1 = new DepartmentDto();
        if (departmentDto.getId()<=0) {
            Department department = modelMapper.map(departmentDto, Department.class);
            Department department1 = departmentRepository.save(department);

            departmentDto1 = modelMapper.map(department1 ,DepartmentDto.class);

        }
        else
            if (departmentDto.getId()>0){
            Department department = departmentRepository.getOne(departmentDto.getId());
            department.setName(departmentDto.getName());
            Department department1 = departmentRepository.save(department);
            departmentDto1 = modelMapper.map(department1 ,DepartmentDto.class);
        }

            return departmentDto1;
    }

    @Override
    public void deleteById(long theId) {
        Optional<Department> result = departmentRepository.findById(theId);
        if (result.isPresent())
        {
            Department department = result.get();
            department.setActive(false);
            department.setSemesters(null);
            department.setStudents(null);
            department.setCourseList(null);
            departmentRepository.save(department);
        }
        else throw new RuntimeException("delete not possible department not found with id = " +theId);

    }
}
