package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.SemesterDto;

import java.util.List;

public interface SemesterService {
    public List<SemesterDto> findAll();

    public SemesterDto findById(long theId);

    public void save(SemesterDto semesterDto);

    public void deleteById(long theId);

    public void saveCoursesToSemester(SemesterDto semesterDto);
}
