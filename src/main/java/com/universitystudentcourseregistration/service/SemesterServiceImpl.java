package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.SemesterDto;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.entity.Semester;
import com.universitystudentcourseregistration.repository.CourseRepository;
import com.universitystudentcourseregistration.repository.DepartmentRepository;
import com.universitystudentcourseregistration.repository.SemesterRepository;
import com.universitystudentcourseregistration.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SemesterServiceImpl implements SemesterService {
    private SemesterRepository semesterRepository;
    private DepartmentRepository departmentRepository;
    private CourseRepository courseRepository;
    private StudentRepository studentRepository ;
    private ModelMapper modelMapper = new ModelMapper();
    @Autowired
    public SemesterServiceImpl(SemesterRepository semesterRepository, DepartmentRepository departmentRepository, CourseRepository courseRepository, StudentRepository studentRepository) {
        this.semesterRepository = semesterRepository;
        this.departmentRepository = departmentRepository;
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public List<SemesterDto> findAll() {
        List<Semester> semesterList = semesterRepository.findByActiveTrue();
        List<SemesterDto> semesterDtoList = new ArrayList<>();
        for (Semester semester :semesterList)
        {
            SemesterDto semesterDto = modelMapper.map(semester , SemesterDto.class);
            semesterDtoList.add(semesterDto);
        }
        return semesterDtoList;
    }

    @Override
    public SemesterDto findById(long theId) {
        Optional<Semester> result = semesterRepository.findById(theId);
        if (result.isPresent())
        {
            Semester semester = result.get();
            return  modelMapper.map(semester,SemesterDto.class);
        }
        else {
            throw new RuntimeException("semester not found by the id = "+ theId);
        }
    }

    @Override
    public void save(SemesterDto semesterDto) {
        if (semesterDto.getId()<=0) {
            Semester semester = modelMapper.map(semesterDto, Semester.class);
            semesterRepository.save(semester);
        }
        else if (semesterDto.getId()>0){
            Semester semester = semesterRepository.getOne(semesterDto.getId());
            semester.setName(semesterDto.getName());
            Department department = departmentRepository.getOne(semesterDto.getDepartment().getId());
            semester.setDepartment(department);
            semesterRepository.save(semester);

        }
    }

    @Override
    public void deleteById(long theId) {
         Optional<Semester> result = semesterRepository.findById(theId);
         if (result.isPresent())
         {
             Semester semester = result.get();
             semester.setActive(false);
             semester.setCourseList(null);
             semester.setDepartment(null);
             semester.setStudentList(null);
             semesterRepository.save(semester);
         }
         else
         {
             throw new RuntimeException("student not found by the id = "+theId);
         }



    }

    @Override
    public void saveCoursesToSemester(SemesterDto semesterDto) {
        Semester semester = semesterRepository.getOne(semesterDto.getId());
        List<Course> courseList = new ArrayList<>();
        semesterDto.getCourseList().forEach(o->{
            Course course = courseRepository.getOne(o.getId());
            courseList.add(course);
        });
        semester.setCourseList(courseList);
        semesterRepository.save(semester);
    }

}
