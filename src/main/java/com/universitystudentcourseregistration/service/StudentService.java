package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.StudentDto;

import java.util.List;

public interface StudentService {

    public List<StudentDto> findAll();

    public StudentDto findById(long theId);

    public void save(StudentDto studentDto);

    public void deleteById(long theId);

    void saveSemesterToStudent(StudentDto studentDto1);
    void saveExtraCoursesToStudent(StudentDto studentDto1);
}
