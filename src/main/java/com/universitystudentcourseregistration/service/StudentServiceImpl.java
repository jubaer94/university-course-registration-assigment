package com.universitystudentcourseregistration.service;

import com.universitystudentcourseregistration.dto.StudentDto;
import com.universitystudentcourseregistration.entity.Course;
import com.universitystudentcourseregistration.entity.Department;
import com.universitystudentcourseregistration.entity.Semester;
import com.universitystudentcourseregistration.entity.Student;
import com.universitystudentcourseregistration.repository.CourseRepository;
import com.universitystudentcourseregistration.repository.DepartmentRepository;
import com.universitystudentcourseregistration.repository.SemesterRepository;
import com.universitystudentcourseregistration.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    private DepartmentRepository departmentRepository;
    private SemesterRepository semesterRepository;
    private CourseRepository courseRepository;
    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, DepartmentRepository departmentRepository, SemesterRepository semesterRepository, CourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.departmentRepository = departmentRepository;
        this.semesterRepository = semesterRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<StudentDto> findAll() {
        List<Student> studentList = studentRepository.findByActiveTrue();
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student :studentList)
        {
            StudentDto studentDto = modelMapper.map(student ,StudentDto.class);
            studentDtoList.add(studentDto);
        }
        return studentDtoList;
    }

    @Override
    public StudentDto findById(long theId) {
        Optional<Student> result = studentRepository.findById(theId);
        if (result.isPresent())
        {
            Student student = result.get();
            return modelMapper.map(student , StudentDto.class);
        }
        else {
            throw new RuntimeException("student not found with id = "+theId);
        }

    }

    @Override
    public void save(StudentDto studentDto) {

        if (studentDto.getId()<=0) {
            Student student = modelMapper.map(studentDto, Student.class);
            studentRepository.save(student);
        }

        else if (studentDto.getId()>0){
            Student student =studentRepository.getOne(studentDto.getId());
            student.setName(studentDto.getName());
            Department department = departmentRepository.getOne(studentDto.getDepartment().getId());
            student.setDepartment(department);
            studentRepository.save(student);
        }


    }

    @Override
    public void deleteById(long theId) {
        Optional<Student> result = studentRepository.findById(theId);
        if (result.isPresent())
        {
            Student student = result.get();
            student.setActive(false);
            student.setSemester(null);
            student.setDepartment(null);
            studentRepository.save(student);

        }
        else {
            throw new RuntimeException("student not found with id = "+theId);
        }

    }

    @Override
    public void saveSemesterToStudent(StudentDto studentDto1) {
        Student student = studentRepository.getOne(studentDto1.getId());
        Semester semester = semesterRepository.getOne(studentDto1.getSemester().getId());
        List<Semester> completedSemesters = student.getCompletedSemesters();
        if (!completedSemesters.contains(semester)) {

            student.setSemester(semester);
            completedSemesters.add(semester);
            student.setCompletedSemesters(completedSemesters);
        }
        else {
            System.out.println("student already  to the course before");
        }

        studentRepository.save(student);
    }

    @Override
    public void saveExtraCoursesToStudent(StudentDto studentDto1) {
        Student student = studentRepository.getOne(studentDto1.getId());
        List<Course> extraCourseList = new ArrayList<>();

        studentDto1.getExtraCourseList().forEach(courseDto -> {
            Course course = courseRepository.getOne(courseDto.getId());
            extraCourseList.add(course);
        });
        student.setExtraCourseList(extraCourseList);

        studentRepository.save(student);

    }
}
