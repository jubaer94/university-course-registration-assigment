$(document).ready(function () {

    $.ajax({
        url: '/students/getAll',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (resp) {

            $.each(resp, function (i, item) {
                var row = `<tr>
                            <td>${item.stuId}</td>
                                 <td>${item.studentName}</td>
                                 <td>${item.depName}</td>
                                 <td>${item.batchName}</td>
                                 <td>
                                 <button type="submit"class="btn btn-success" data-dismiss="modal"onclick="editStudent(${item.stuId})">Edit Info</button>
                                 <button type="submit"class="btn btn-danger" data-dismiss="modal"onclick="deleteStudent(${item.stuId})">Delete</button>
                                  </td>
                        </tr>`;
                $(".table-body").append(row);
            });
        },
        error: function (err) {
            console.log(err);
        }
    });
})